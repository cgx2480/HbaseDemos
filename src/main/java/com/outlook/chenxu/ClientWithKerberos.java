package com.outlook.chenxu;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.security.UserGroupInformation;

import java.io.IOException;

public class ClientWithKerberos {
    public static void main(String[] args) {

        System.setProperty("java.security.krb5.conf",
                "D:\\资料\\IdeaProjects\\HbaseDemos\\src\\main\\resources\\krb5.conf");
        Configuration configuration = HBaseConfiguration.create();
        System.out.println("hbase.rootdir : " + configuration.get("hbase.rootdir"));
        System.out.println(configuration.toString());
        configuration.set("hadoop.security.authentication", "Kerberos");
        UserGroupInformation.setConfiguration(configuration);

        try {
            UserGroupInformation.loginUserFromKeytab("hbase@SIRC.COM",
                    "D:\\资料\\IdeaProjects\\HbaseDemos\\src\\main\\resources\\hbase.keytab");

            Connection connection = ConnectionFactory.createConnection(configuration);
            Table table = connection.getTable(TableName.valueOf("inserttable"));
            System.out.println(table.getName());
            Scan scan = new Scan();
            ResultScanner rs = table.getScanner(scan);
            for (Result r : rs) {
                System.out.println(r.toString());
//                break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
